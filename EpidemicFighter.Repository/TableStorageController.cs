﻿using System;
using System.Collections.Generic;
using Microsoft.Azure.Cosmos.Table;

namespace EpidemicFighter.Repository
{
    public class TableStorageController<T> : ITableStorageController<T> where T : class, ITableEntity, new()
    {
        /// <summary>
        /// Table client.
        /// </summary>
        protected readonly CloudTable _tableClient;
        public TableStorageController(CloudTable tableClient)
        {
            _tableClient = tableClient;
        }

        /// <summary>
        /// Insert or merge an entity into the Table Storage.
        /// </summary>
        /// <param name="entity">T Entity which should be inserted or merged.</param>
        /// <returns>Returns the inserted or merged entity.</returns>
        public T InsertOrMergeEntity(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"The param entity is not initialized!");
            }
            try
            {
                TableOperation insertOrMergeOperation = TableOperation.InsertOrMerge(entity);

                TableResult result = _tableClient.Execute(insertOrMergeOperation);

                if (!(result.Result is T insertedCustomer))
                {
                    throw new Exception("Table insert operation failed!");
                }
                return insertedCustomer;
            }
            catch (StorageException e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Retrives the entity, using point query.
        /// </summary>
        /// <param name="partitionKey">Partition key</param>
        /// <param name="rowKey">Row key</param>
        /// <returns>Returns with the entity.</returns>
        public T? RetrieveEntityUsingPointQuery(string partitionKey, string rowKey)
        {
            try
            {
                TableOperation retrieveOperation = TableOperation.Retrieve<T>(partitionKey, rowKey);
                TableResult result = _tableClient.Execute(retrieveOperation);
                var entity = result?.Result as T;

                return entity;
            }
            catch (StorageException e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Delete entity.
        /// </summary>
        /// <param name="deleteEntity">Entity which should be deleted.</param>
        public void DeleteEntity(T deleteEntity)
        {
            try
            {
                if (deleteEntity == null)
                {
                    throw new ArgumentNullException("The param entity is not initialized!");
                }
                TableOperation deleteOperation = TableOperation.Delete(deleteEntity);
                TableResult result = _tableClient.Execute(deleteOperation);
            }
            catch (StorageException e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Get all entity, filtered with custom query.
        /// </summary>
        /// <param name="tableQuery">Table query</param>
        /// <returns>Returns with a list of entity.</returns>
        public virtual List<T> GetAllEntity(TableQuery<T> tableQuery)
        {

            TableContinuationToken? token = null;

            var entities = new List<T>();

            do
            {
                var queryResult = _tableClient.ExecuteQuerySegmented(tableQuery, token);
                entities.AddRange(queryResult.Results);
                token = queryResult.ContinuationToken;
            } while (token != null);

            return entities;
        }

    }
}
