﻿using System.Collections.Generic;

namespace EpidemicFighter.Repository
{
    public interface IScoreController
    {
        /// <summary>
        /// Inserts or updates ScoreEntity to the Table Storage.
        /// </summary>
        /// <param name="entity">ScoreEntity which should be inserted into the table or updated in the table.</param>
        /// <returns>The inserted or updated entity.</returns>
        ScoreEntity AddOrUpdateScore(ScoreEntity entity);

        /// <summary>
        /// Inserts a new score entity into the table.
        /// </summary>
        /// <param name="nickname">Name of the player.</param>
        /// <param name="point">Reached score of the player.</param>
        /// <returns>Returns the inserted score entity,</returns>
        ScoreEntity AddScore(string nickname, int point);

        /// <summary>
        /// Retrives an entity.
        /// </summary>
        /// <param name="entity">Sample entity.</param>
        /// <returns>Returns the entity.</returns>

        ScoreEntity? GetScore(ScoreEntity entity);

        /// <summary>
        /// Gets the top 5 scores from the Table Storage.
        /// </summary>
        /// <returns>Returns a list of ScoreEntity.</returns>
        List<ScoreEntity> GetTop5();
    }
}