﻿using Microsoft.Azure.Cosmos.Table;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EpidemicFighter.Repository
{
    public class ScoreController : TableStorageController<ScoreEntity>, IScoreController
    {
        /// <summary>
        /// Constant partition key of the Table Storage.
        /// </summary>
        private const string PartitionKey = "player";
        public ScoreController(CloudTable tableClient) : base(tableClient)
        {
        }

        /// <summary>
        /// Inserts or updates ScoreEntity to the Table Storage.
        /// </summary>
        /// <param name="entity">ScoreEntity which should be inserted into the table or updated in the table.</param>
        /// <returns>The inserted or updated entity.</returns>
        public ScoreEntity AddOrUpdateScore(ScoreEntity entity)
        {

            var dbScore = RetrieveEntityUsingPointQuery(PartitionKey, entity.RowKey);
            if (dbScore == null)
            {
                dbScore = InsertOrMergeEntity(entity);
                return dbScore;
            }
            else if (entity.Equals(dbScore))
            {
                return dbScore;
            }
            else
            {
                dbScore.Nickname = entity.Nickname;
                dbScore.Point = entity.Point;
                dbScore = InsertOrMergeEntity(dbScore);

                return dbScore;
            }
        }

        /// <summary>
        /// Inserts a new score entity into the table.
        /// </summary>
        /// <param name="nickname">Name of the player.</param>
        /// <param name="point">Reached score of the player.</param>
        /// <returns>Returns the inserted score entity,</returns>
        public ScoreEntity AddScore(string nickname, int point)
        {
            var newScore = new ScoreEntity(Guid.NewGuid().ToString(), nickname, point);
            return AddOrUpdateScore(newScore);
        }

        /// <summary>
        /// Gets the top 5 scores from the Table Storage.
        /// </summary>
        /// <returns>Returns a list of ScoreEntity.</returns>
        public List<ScoreEntity>GetTop5()
        {
            TableQuery<ScoreEntity> query = new TableQuery<ScoreEntity>().Take(100);
            var scores = (GetAllEntity(query)).OrderByDescending(x => x.Point).Take(5);
            return scores.ToList();
        }

        /// <summary>
        /// Retrives an entity.
        /// </summary>
        /// <param name="entity">Sample entity.</param>
        /// <returns>Returns the entity.</returns>
        public ScoreEntity? GetScore(ScoreEntity entity)
        {

            var dbScore = RetrieveEntityUsingPointQuery(PartitionKey, entity.RowKey);
            return dbScore;

        }
    }
}