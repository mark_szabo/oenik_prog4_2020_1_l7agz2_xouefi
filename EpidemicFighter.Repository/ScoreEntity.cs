﻿using Microsoft.Azure.Cosmos.Table;
using Newtonsoft.Json;

namespace EpidemicFighter.Repository
{
    public class ScoreEntity : TableEntity
    {
        public ScoreEntity() { }

        public ScoreEntity(string guid)
        {
            PartitionKey = "player";
            RowKey = guid;
        }

        public ScoreEntity(string guid, string nickname, int point)
        {
            PartitionKey = "player";
            RowKey = guid;
            this.Nickname = nickname;
            this.Point = point;
        }

        /// <summary>
        /// Nick name of the player.
        /// </summary>
        [JsonProperty("nickname")]
        public string? Nickname { get; set; }

        /// <summary>
        /// Reached score of the player.
        /// </summary>
        [JsonProperty("point")]
        public int? Point { get; set; }
    }
}
