﻿using Microsoft.Azure.Cosmos.Table;
using System.Collections.Generic;

namespace EpidemicFighter.Repository
{
    public interface ITableStorageController<T> where T : class, ITableEntity, new()
    {
        /// <summary>
        /// Delete entity.
        /// </summary>
        /// <param name="deleteEntity">Entity which should be deleted.</param>
        void DeleteEntity(T deleteEntity);

        /// <summary>
        /// Get all entity, filtered with custom query.
        /// </summary>
        /// <param name="tableQuery">Table query</param>
        /// <returns>Returns with a list of entity.</returns>
        List<T> GetAllEntity(TableQuery<T> tableQuery);

        /// <summary>
        /// Insert or merge an entity into the Table Storage.
        /// </summary>
        /// <param name="entity">T Entity which should be inserted or merged.</param>
        /// <returns>Returns the inserted or merged entity.</returns>
        T InsertOrMergeEntity(T entity);

        /// <summary>
        /// Retrives the entity, using point query.
        /// </summary>
        /// <param name="partitionKey">Partition key</param>
        /// <param name="rowKey">Row key</param>
        /// <returns>Returns with the entity.</returns>
        T? RetrieveEntityUsingPointQuery(string partitionKey, string rowKey);
    }
}