﻿using EpidemicFighter.Repository;
using Microsoft.Azure.Cosmos.Table;
using System.Collections.Generic;

namespace EpidemicFighter.Logic
{
    public class Score
    {
        /// <summary>
        /// ScoreController to connect to Azure Table Storage.
        /// </summary>
        private readonly IScoreController _scoreController;

        public Score(CloudTable tableClient)
        {
            _scoreController = new ScoreController(tableClient);
        }

        /// <summary>
        /// Add score to the Cloud Table.
        /// </summary>
        /// <param name="name">Nickname of the player.</param>
        /// <param name="point">Reached score.</param>
        public void AddScore(string name, int point)
        {
            _scoreController.AddScore(name, point);
        }

        /// <summary>
        /// Get the top 5 score from the Cloud Table.
        /// </summary>
        /// <returns>List of ScoreEntity.</returns>
        public List<ScoreEntity> GetScore()
        {
            return _scoreController.GetTop5();
        }

    }
}
