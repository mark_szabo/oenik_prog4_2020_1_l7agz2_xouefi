﻿using System.Windows.Media;

namespace EpidemicFighter.Logic
{
    /// <summary>
    /// Constants.
    /// </summary>
    public static class Constants
    {
        public const double Width = 1500;
        public const double Height = 1000;
        public static readonly Brush CellColor = Brushes.Azure;
        public static readonly Brush ImmuneCellColor = Brushes.DeepSkyBlue;
        public static readonly Brush InfectedCellColor = Brushes.Crimson;
        public static readonly Brush SelectedCellColor = Brushes.Chartreuse;
        public static readonly Brush BackgroundColor = Brushes.Black;
        public const int CellSize = 40;
        public const double StrenghteningPerTick = 0.005;
        public const double MinStrengthToStartAttact = 0.4;
        public const double Speed = 10;
        public const double AttackStrengthPrecentage = 0.5;
    }
}
