﻿using System.Text.Json.Serialization;
using System.Windows;
using System.Windows.Media;

namespace EpidemicFighter.Logic
{
    /// <summary>
    /// Generic GameItem abstract base class.
    /// </summary>
    public abstract class GameItem
    {
        /// <summary>
        /// Cache for storing the last calculated area.
        /// (X, Y, area)
        /// </summary>
        private (double, double, Geometry)? _positionCache = null;

        /// <summary>
        /// Area of the game item.
        /// </summary>
        [JsonIgnore]
        protected Geometry? Area { get; set; }

        /// <summary>
        /// Center position X coordinate
        /// </summary>
        public double X { get; set; }

        /// <summary>
        /// Center position Y coordinate
        /// </summary>
        public double Y { get; set; }

        /// <summary>
        /// Center position.
        /// </summary>
        [JsonIgnore]
        public Point Center
        {
            get => new Point(X, Y);
        }

        /// <summary>
        /// Gets the area of the game item. Computationally expensive task.
        /// </summary>
        /// <returns>Area of the game item.</returns>
        public Geometry? GetArea()
        {
            if (Area == null) return null;

            if (_positionCache != null)
            {
                var (xCache, yCache, areaCache) = _positionCache.Value;
                if (X == xCache && Y == yCache) return areaCache;
            }

            var transfromGroup = new TransformGroup();
            transfromGroup.Children.Add(new TranslateTransform(X, Y));
            Area.Transform = transfromGroup;
            var area = Area.GetFlattenedPathGeometry();
            _positionCache = (X, Y, area);

            return area;
        }

        /// <summary>
        /// Gets the distance between two game items.
        /// </summary>
        /// <param name="item">Other game item.</param>
        /// <returns>Distance between the two game items.</returns>
        public double DistanceFrom(GameItem item) => Point.Subtract(Center, item.Center).Length;

        /// <summary>
        /// Collision calculation. Computationally expensive task.
        /// </summary>
        /// <param name="item">Other game item.</param>
        /// <returns>True if the items collide.</returns>
        public bool CollidesWith(GameItem item) => Geometry
                .Combine(GetArea(), item.GetArea(), GeometryCombineMode.Intersect, null)
                .GetArea() > 0;

        /// <summary>
        /// Clears position cache to trigger re-render.
        /// </summary>
        protected void ClearPositionCache()
        {
            _positionCache = null;
        }
    }
}
