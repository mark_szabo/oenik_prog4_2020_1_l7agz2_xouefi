using System;
using System.Windows;

namespace EpidemicFighter.Logic
{
    /// <summary>
    /// Game logic.
    /// </summary>
    public interface IGameLogic
    {
        /// <summary>
        /// Triggers a re-render.
        /// </summary>
        event EventHandler? Render;

        /// <summary>
        /// Triggers the next level.
        /// </summary>
        event EventHandler? NextLevel;

        /// <summary>
        /// Triggers the gameover.
        /// </summary>
        event EventHandler? GameOver;

        /// <summary>
        /// Strengthen active cells, trigger enemy attacks randomly, process in-progress attacks and trigger rerenders.
        /// </summary>
        void Tick();

        /// <summary>
        /// Checks whether the first cell is connected to the second.
        /// 
        /// Time complexity: O(n)
        /// </summary>
        /// <param name="cell1">The first cell to be checked against.</param>
        /// <param name="cell2">The second cell.</param>
        /// <returns>True if cells are connected.</returns>
        bool AreCellsConnected(Cell cell1, Cell cell2);

        /// <summary>
        /// Adds a link between two cells.
        /// 
        /// Time complexity: O(n)
        /// </summary>
        /// <param name="cell1">The first cell.</param>
        /// <param name="cell2">The second cell.</param>
        void AddLink(Cell cell1, Cell cell2);

        /// <summary>
        /// Removes a link between two cells.
        /// 
        /// Time complexity: O(n)
        /// </summary>
        /// <param name="cell1">The first cell.</param>
        /// <param name="cell2">The second cell.</param>
        void RemoveLink(Cell cell1, Cell cell2);

        /// <summary>
        /// Removes all links of a cells.
        /// 
        /// Time complexity: O(n)
        /// </summary>
        /// <param name="cell">The cell.</param>
        void RemoveAllLink(Cell cell);
        void ComputeDistances();

        /// <summary>
        /// Gets the clicked cell from the coordinates of the click.
        /// </summary>
        /// <param name="point">Coordinates of the click.</param>
        /// <returns>The clicked cell or null if no cell was clicked.</returns>
        Cell? GetClickedCell(Point point);

        /// <summary>
        /// Handles a click on a cell.
        /// </summary>
        /// <param name="cell">The clicked cell.</param>
        void OnCellClickHandler(Cell cell);

        /// <summary>
        /// Computer attack logic, uses InfectableCell(CellState state), SupportInfectedCell(),
        /// ConnectInfectedToInfected().
        /// </summary>
        void ComputerAttack();
    }
}