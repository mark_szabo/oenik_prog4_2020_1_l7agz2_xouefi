﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace EpidemicFighter.Logic
{
    public class SavedGameModel
    {
        public SavedGameModel(GameModel model)
        {
            Nickname = model.Nickname;
            Level = model.Level;
            EllapsedSeconds = model.EllapsedSeconds;
            Cells = model.Cells;
            Links = model.Links.ToDictionary(link => link.Key.Id, link => link.Value.ToDictionary(l => l.Key.Id, l => l.Value));
            AttackLinks = model.AttackLinks;
            SelectedCell = model.SelectedCell;
            GameWidth = model.GameWidth;
            GameHeight = model.GameHeight;
        }

        public SavedGameModel() { }

        /// <summary>
        /// Nickname of the gamer.
        /// </summary>
        public string? Nickname { get; set; }

        /// <summary>
        /// Current level.
        /// </summary>
        public int Level { get; set; }

        /// <summary>
        /// Ellapsed seconds since level start.
        /// </summary>
        public int EllapsedSeconds { get; set; } = 0;

        /// <summary>
        /// List of cells (nodes) visible on the screen.
        /// </summary>
        public List<Cell> Cells { get; } = new List<Cell>();

        /// <summary>
        /// Dictionary containing the links of the game.
        /// Keys are cells and each contain all of the links the given cell has in another dictionary.
        /// </summary>
        public Dictionary<string, Dictionary<string, Link>> Links { get; set; } = new Dictionary<string, Dictionary<string, Link>>();

        /// <summary>
        /// List of permanent attack/support links.
        /// </summary>
        public List<AttackLink> AttackLinks { get; set; } = new List<AttackLink>();

        /// <summary>
        /// The currently selected cell (if any).
        /// </summary>
        public Cell? SelectedCell { get; set; }

        /// <summary>
        /// Width of the game area.
        /// </summary>
        public double GameWidth { get; }

        /// <summary>
        /// Height of the game area.
        /// </summary>
        public double GameHeight { get; }

        /// <summary>
        /// Convert to game model.
        /// </summary>
        /// <returns>GameModel</returns>
        public GameModel ToGameModel()
        {
            var map = new Map { Cells = this.Cells };
            var model = new GameModel(map, this.GameWidth, this.GameHeight)
            {
                Nickname = this.Nickname,
                Level = this.Level,
                EllapsedSeconds = this.EllapsedSeconds,
                Links = this.Links.ToDictionary(link => GetCell(link.Key), link => link.Value.ToDictionary(l => GetCell(l.Key), l => l.Value)),
                AttackLinks = this.AttackLinks
            };

            model.AttackLinks.ForEach(l =>
            {
                l.Cell1 = model.Cells.Single(c => c.X == l.Cell1.X && c.Y == l.Cell1.Y);
                l.Cell2 = model.Cells.Single(c => c.X == l.Cell2.X && c.Y == l.Cell2.Y);
            });

            model.SelectedCell = model.Cells.SingleOrDefault(c => c.X == SelectedCell?.X && c.Y == SelectedCell?.Y);

            return model;
        }

        /// <summary>
        /// Serializes the model to JSON.
        /// </summary>
        /// <returns></returns>
        public string? ToJson(Stopwatch stopwatch)
        {
            EllapsedSeconds = Convert.ToInt32(stopwatch.ElapsedMilliseconds / 1000);
            return JsonConvert.SerializeObject(this);
        }

        /// <summary>
        /// Get cell based on id.
        /// </summary>
        /// <param name="id">Id of a cell.</param>
        /// <returns> Cell based on ID.</returns>
        private Cell GetCell(string id)
        {
            var (x, y) = JsonConvert.DeserializeObject<(double, double)>(id);
            return Cells.Single(c => c.X == x && c.Y == y);
        }
    }
}
