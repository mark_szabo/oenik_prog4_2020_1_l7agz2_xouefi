﻿using System.Windows;
using System.Windows.Media;

namespace EpidemicFighter.Logic
{
    /// <summary>
    /// Attack/support from one cell to another.
    /// </summary>
    public class AttackLink : Link
    {
        public AttackLink(Cell cell1, Cell cell2, double strength) : base(cell1, cell2)
        {
            Progress = Constants.Speed / Distance;
            Strength = strength;

            var geometry = new GeometryGroup();
            geometry.Children.Add(new LineGeometry(cell1.Center, cell2.Center));
            Area = geometry;
        }

        /// <summary>
        /// Progress precentage. Between 0 and 1.
        /// </summary>
        public double Progress { get; private set; }

        /// <summary>
        /// Strength of the attack.
        /// </summary>
        public double Strength { get; }

        /// <summary>
        /// Advance teh attack/support be a predefined, constant unit.
        /// </summary>
        public void Advance()
        {
            Progress += Constants.Speed / Distance;

            var vector = Point.Subtract(Cell2.Center, Cell1.Center);
            vector.X *= Progress;
            vector.Y *= Progress;
            var lineEndPoint = new Point(Cell1.X + vector.X, Cell1.Y + vector.Y);

            var geometry = new GeometryGroup();
            geometry.Children.Add(new LineGeometry(Cell1.Center, lineEndPoint));
            Area = geometry;

            ClearPositionCache();
        }
    }
}
