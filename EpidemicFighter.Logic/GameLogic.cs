﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace EpidemicFighter.Logic
{
    /// <summary>
    /// Game logic.
    /// </summary>
    public class GameLogic : IGameLogic
    {
        /// <summary>
        /// Random generator.
        /// </summary>
        private static readonly Random rnd = new Random();

        private readonly GameModel _model;

        /// <summary>
        /// Triggers a re-render.
        /// </summary>
        public event EventHandler? Render;

        /// <summary>
        /// Triggers the gameover
        /// </summary>
        public event EventHandler? GameOver;

        /// <summary>
        /// Triggers the next level
        /// </summary>
        public event EventHandler? NextLevel;

        public GameLogic(GameModel model)
        {
            _model = model;
        }

        /// <summary>
        /// Strengthen active cells, trigger enemy attacks randomly, process in-progress attacks and trigger rerenders.
        /// </summary>
        public void Tick()
        {
            foreach (var cell in _model.Cells)
            {
                cell.Strengthen();
            }

            foreach (var attackLink in _model.AttackLinks)
            {
                attackLink.Advance();

                if (attackLink.Progress >= 1)
                {
                    if (attackLink.Cell1.State == attackLink.Cell2.State)
                    {
                        attackLink.Cell2.Strength += attackLink.Strength;
                        if (!AreCellsConnected(attackLink.Cell1, attackLink.Cell2)) AddLink(attackLink.Cell1, attackLink.Cell2);
                    }
                    else
                    {
                        attackLink.Cell2.Strength -= attackLink.Strength;

                        if (attackLink.Cell2.Strength < 0)
                        {
                            RemoveAllLink(attackLink.Cell2);
                            AddLink(attackLink.Cell1, attackLink.Cell2);
                            attackLink.Cell2.State = attackLink.Cell1.State;
                            attackLink.Cell2.Strength *= -1;
                            GameStatusCheck();
                        }
                    }
                }
            }

            _model.AttackLinks.RemoveAll(l => l.Progress >= 1);

            Render?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// Checks whether the first cell is connected to the second.
        /// 
        /// Time complexity: O(n)
        /// </summary>
        /// <param name="cell1">The first cell to be checked against.</param>
        /// <param name="cell2">The second cell.</param>
        /// <returns>True if cells are connected.</returns>
        public bool AreCellsConnected(Cell cell1, Cell cell2)
        {
            Dictionary<Cell, Link> connectedCells;
            try
            {
                connectedCells = _model.Links[cell1];
            }
            catch (KeyNotFoundException)
            {
                return false;
            }

            return connectedCells.ContainsKey(cell2);
        }

        /// <summary>
        /// Adds a link between two cells.
        /// 
        /// Time complexity: O(n)
        /// </summary>
        /// <param name="cell1">The first cell.</param>
        /// <param name="cell2">The second cell.</param>
        public void AddLink(Cell cell1, Cell cell2)
        {
            AddLinkToDictionary(cell1, cell2);
            AddLinkToDictionary(cell2, cell1);
        }

        /// <summary>
        /// Removes a link between two cells.
        /// 
        /// Time complexity: O(n)
        /// </summary>
        /// <param name="cell1">The first cell.</param>
        /// <param name="cell2">The second cell.</param>
        public void RemoveLink(Cell cell1, Cell cell2)
        {
            try
            {
                var _ = _model.Links[cell1];
            }
            catch (KeyNotFoundException)
            {
                return;
            }

            RemoveLinkFromDictionary(cell1, cell2);
            RemoveLinkFromDictionary(cell2, cell1);
        }

        /// <summary>
        /// Removes all links of a cells.
        /// 
        /// Time complexity: O(n)
        /// </summary>
        /// <param name="cell">The cell.</param>
        public void RemoveAllLink(Cell cell)
        {
            try
            {
                var connectedCells = _model.Links[cell];

                foreach (var connectedCell in connectedCells)
                {
                    RemoveLinkFromDictionary(cell, connectedCell.Key);
                    RemoveLinkFromDictionary(connectedCell.Key, cell);
                }
            }
            catch (KeyNotFoundException)
            {
                return;
            }
        }

        /// <summary>
        /// Gets the clicked cell from the coordinates of the click.
        /// </summary>
        /// <param name="point">Coordinates of the click.</param>
        /// <returns>The clicked cell or null if no cell was clicked.</returns>
        public Cell? GetClickedCell(Point point)
        {
            foreach (var cell in _model.Cells)
            {
                if (cell.GetArea()?.FillContains(point) ?? false) return cell;
            }

            return null;
        }

        /// <summary>
        /// Handles a click on a cell.
        /// </summary>
        /// <param name="cell">The clicked cell.</param>
        public void OnCellClickHandler(Cell cell)
        {
            if (_model.SelectedCell == null)
            {
                _model.SelectedCell = cell;
            }
            else
            {
                if (_model.SelectedCell.CanAttack(cell)) Attack(_model.SelectedCell, cell);
                _model.SelectedCell = null;
            }
        }

        /// <summary>
        /// Calculates the dostance between cells.
        /// </summary>
        public void ComputeDistances()
        {
            var cellDistances = new Dictionary<Dictionary<Cell, Cell>, double>();

            foreach (var cellA in _model.Cells)
            {
                var otherCells = _model.Cells.Where(x => x != cellA);

                foreach (var cellB in otherCells)
                {
                    if (!cellDistances.ContainsKey(new Dictionary<Cell, Cell>() {
                        { cellA, cellB } }) &&
                        !cellDistances.ContainsKey(new Dictionary<Cell, Cell>() {
                        { cellB, cellA } }))
                    {
                        var distance = cellA.DistanceFrom(cellB);

                        cellDistances.Add(new Dictionary<Cell, Cell>() {
                        { cellA, cellB } }, distance);

                        cellDistances.Add(new Dictionary<Cell, Cell>() {
                        { cellB, cellA } }, distance);
                    }
                }
            }

            _model.Distances = cellDistances;
        }

        /// <summary>
        /// Starts an attack or support from one cell to another.
        /// </summary>
        /// <param name="from">Source cell.</param>
        /// <param name="to">Destination cell.</param>
        private void Attack(Cell from, Cell to)
        {
            var strength = from.Strength * Constants.AttackStrengthPrecentage;
            from.Strength -= strength;
            _model.AttackLinks.Add(new AttackLink(from, to, strength));
        }

        /// <summary>
        /// Adds the connection to the hashset of cell1 in the link dictionary.
        /// This is needed to be called for both of the cells in a link.
        /// 
        /// Time complexity: O(n)
        /// </summary>
        /// <param name="cell1">The first cell.</param>
        /// <param name="cell2">The second cell.</param>
        private void AddLinkToDictionary(Cell cell1, Cell cell2)
        {
            Dictionary<Cell, Link> connectedCells;
            try
            {
                connectedCells = _model.Links[cell1];
                connectedCells.Add(cell2, new Link(cell1, cell2));
            }
            catch (KeyNotFoundException)
            {
                connectedCells = new Dictionary<Cell, Link> { { cell2, new Link(cell1, cell2) } };
                _model.Links.Add(cell1, connectedCells);
            }
        }

        /// <summary>
        /// Removes the connection from the hashset of cell1 in the link dictionary.
        /// This is needed to be called for both of the cells in a link.
        /// 
        /// Time complexity: O(n)
        /// </summary>
        /// <param name="cell1">The first cell.</param>
        /// <param name="cell2">The second cell.</param>
        private void RemoveLinkFromDictionary(Cell cell1, Cell cell2)
        {
            try
            {
                var connectedCells = _model.Links[cell1];

                connectedCells.Remove(cell2);
            }
            catch (KeyNotFoundException)
            {
                return;
            }
        }

        /// <summary>
        /// Querying the infected cells based on the state of the cells and returns one pair.
        /// </summary>
        /// <param name="state">State of the cells.</param>
        /// <returns>Returns two cells which can attack eachother. Can be null.</returns>
        private (Cell, Cell)? InfectableCell(CellState state)
        {
            var infectedCells = _model.Cells.Where(x => x.State == CellState.Infected && x.Strength > Constants.MinStrengthToStartAttact);
            var infectableCells = _model.Cells.Where(x => x.State == state);
            var infectedToAny = _model.Distances.Where(x => infectedCells.Contains(x.Key.Keys.FirstOrDefault()) &&
                               infectableCells.Contains(x.Key.Values.FirstOrDefault()))
                   .OrderBy(x => x.Value)
                   .Take(5).ToList();
            if ((infectedToAny.Count - 1) >= 0)
            {
                var infToPass = infectedToAny[rnd.Next(0, (infectedToAny.Count - 1))].Key;
                return (infToPass.Keys.FirstOrDefault(), infToPass.Values.FirstOrDefault());
            }
            return null;
        }

        /// <summary>
        /// Querying the supportable cells and returns one pair.
        /// </summary>
        /// <returns>Returns two cells which can support eachother. Can be null.</returns>
        private (Cell, Cell)? SupportInfectedCell()
        {
            var infectedCells = _model.Cells.Where(x => x.State == CellState.Infected && x.Strength > Constants.MinStrengthToStartAttact && x.Strength < Constants.MinStrengthToStartAttact+0.20).ToList();
            if (infectedCells.Count >= 3)
            {
                var supportedCell = _model.Links[infectedCells[rnd.Next(0, infectedCells.Count - 1)]];
                return (supportedCell.Values.FirstOrDefault().Cell1, supportedCell.Values.FirstOrDefault().Cell2);
            }
            return null;
        }

        /// <summary>
        /// Querying the infected cells which not connected directly with eachother and returns with one pair.
        /// </summary>
        /// <returns>Returns two cells which can be connected with eachother. Can be null.</returns>
        private (Cell, Cell)? ConnectInfectedToInfected()
        {
            var infectedCells = _model.Cells.Where(x => x.State == CellState.Infected && x.Strength > Constants.MinStrengthToStartAttact).ToList();
            if (infectedCells.Count >= 3)
            {
                var randCell1 = infectedCells[rnd.Next(0, infectedCells.Count - 1)];
                var randCell2 = infectedCells[rnd.Next(0, infectedCells.Count - 1)];

                if (AreCellsConnected(randCell1, randCell2))
                {
                    return (randCell1, randCell2);
                }
            }
            return null;
        }

        /// <summary>
        /// Checks that if the level is completed or the computer won, If completed then triggers the next level.
        /// If the computer won then triggers the game over page.
        /// </summary>
        private void GameStatusCheck()
        {
            var numOfInfected = _model.Cells.Where(x => x.State == CellState.Infected).Count();
            var numOfPassive = _model.Cells.Where(x => x.State == CellState.Passive).Count();
            var numOfImmune = _model.Cells.Where(x => x.State == CellState.Immune).Count();
            if (numOfInfected == 0)
                NextLevel?.Invoke(this, EventArgs.Empty);
            else if (numOfImmune == 0)
                GameOver?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// Computer attack logic, uses InfectableCell(CellState state), SupportInfectedCell(),
        /// ConnectInfectedToInfected().
        /// </summary>
        public void ComputerAttack()
        {
            if (_model.Cells.Where(x => x.State == CellState.Passive).Count() > 0)
            {
                var infectedToPassive = InfectableCell(CellState.Passive);
                if (infectedToPassive != null)
                {
                    Attack(infectedToPassive.Value.Item1, infectedToPassive.Value.Item2);
                }

                if (rnd.NextDouble() < 0.2)
                {
                    infectedToPassive = InfectableCell(CellState.Passive);
                    if (infectedToPassive != null)
                    {
                        Attack(infectedToPassive.Value.Item1, infectedToPassive.Value.Item2);
                    }
                }

                if (rnd.NextDouble() < 0.3)
                {
                    var infectedToImmune = InfectableCell(CellState.Immune);
                    infectedToImmune = InfectableCell(CellState.Immune);
                    if (infectedToImmune != null)
                    {
                        Attack(infectedToImmune.Value.Item1, infectedToImmune.Value.Item2);
                    }
                }

                if (rnd.NextDouble() < 0.1)
                {
                    var supportInfectedCell = SupportInfectedCell();
                    if (supportInfectedCell != null)
                    {
                        Attack(supportInfectedCell.Value.Item1, supportInfectedCell.Value.Item2);
                    }
                }

                if (rnd.NextDouble() < 0.8)
                {
                    var connect2InfectedCell = ConnectInfectedToInfected();
                    if (connect2InfectedCell != null)
                    {
                        Attack(connect2InfectedCell.Value.Item1, connect2InfectedCell.Value.Item2);
                    }
                }
            }
            else
            {
                var infectedToImmune = InfectableCell(CellState.Immune);
                if (infectedToImmune != null)
                {
                    Attack(infectedToImmune.Value.Item1, infectedToImmune.Value.Item2);

                    
                    if (rnd.NextDouble() < 0.5)
                    {
                        infectedToImmune = InfectableCell(CellState.Immune);
                        if (infectedToImmune != null)
                        {
                            Attack(infectedToImmune.Value.Item1, infectedToImmune.Value.Item2);
                        }
                    }
                    if (rnd.NextDouble() < 0.1)
                    {
                        var supportInfectedCell = SupportInfectedCell();
                        if (supportInfectedCell != null)
                        {
                            Attack(supportInfectedCell.Value.Item1, supportInfectedCell.Value.Item2);
                        }
                    }

                    if (rnd.NextDouble() < 0.8)
                    {
                        var connect2InfectedCell = ConnectInfectedToInfected();
                        if (connect2InfectedCell != null)
                        {
                            Attack(connect2InfectedCell.Value.Item1, connect2InfectedCell.Value.Item2);
                        }
                    }
                }
            }
        }
    }
}
