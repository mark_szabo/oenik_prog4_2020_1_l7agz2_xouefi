﻿using System.Collections.Generic;
using System.Text.Json;

namespace EpidemicFighter.Logic
{
    /// <summary>
    /// A signgle game map.
    /// </summary>
    public class Map
    {
        /// <summary>
        /// List of cells (nodes) visible on the screen.
        /// </summary>
        public List<Cell> Cells { get; set; } = new List<Cell>();

        /// <summary>
        /// Maximum reachable score on this map.
        /// </summary>
        public int MaxScore { get; set; }

        /// <summary>
        /// Serializis the map to JSON.
        /// </summary>
        /// <returns></returns>
        public override string? ToString() => JsonSerializer.Serialize(this);
    }
}