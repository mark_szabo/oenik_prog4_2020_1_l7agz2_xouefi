﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace EpidemicFighter.Logic
{
    /// <summary>
    /// Game model.
    /// </summary>
    public class GameModel
    {
        public GameModel(Map map, double gameWidth, double gameHeight)
        {
            Cells = map.Cells;
            GameWidth = gameWidth;
            GameHeight = gameHeight;
        }

        public GameModel(Map map, double gameWidth, double gameHeight, string name)
        {
            Cells = map.Cells;
            GameWidth = gameWidth;
            GameHeight = gameHeight;
            Nickname = name;
        }

        public GameModel()
        {
        }

        public GameModel(GameModel model, double gameWidth, double gameHeight)
        {
            this.Nickname = model.Nickname;
            this.Level = model.Level;
            this.Cells = model.Cells;
            this.Links = model.Links;
            this.Distances = model.Distances;
            this.AttackLinks = model.AttackLinks;
            this.SelectedCell = model.SelectedCell;
            this.GameWidth = gameWidth;
            this.GameHeight = gameHeight;
        }

        /// <summary>
        /// Nickname of the gamer.
        /// </summary>
        public string? Nickname { get; set; }

        /// <summary>
        /// Current level.
        /// </summary>
        public int Level { get; set; }

        /// <summary>
        /// Ellapsed seconds since level start.
        /// </summary>
        public int EllapsedSeconds { get; set; } = 0;

        /// <summary>
        /// List of cells (nodes) visible on the screen.
        /// </summary>
        public List<Cell> Cells { get; } = new List<Cell>();

        /// <summary>
        /// Dictionary containing the links of the game.
        /// Keys are cells and each contain all of the links the given cell has in another dictionary.
        /// </summary>
        public Dictionary<Cell, Dictionary<Cell, Link>> Links { get; set; } = new Dictionary<Cell, Dictionary<Cell, Link>>();

        /// <summary>
        /// Dictionary containing the distances between cells.
        /// Keys are dictionary from cells.
        /// </summary>
        [JsonIgnore]
        public Dictionary<Dictionary<Cell, Cell>, double> Distances { get; set; } = new Dictionary<Dictionary<Cell, Cell>, double>();

        /// <summary>
        /// List of permanent attack/support links.
        /// </summary>
        public List<AttackLink> AttackLinks { get; set; } = new List<AttackLink>();

        /// <summary>
        /// The currently selected cell (if any).
        /// </summary>
        public Cell? SelectedCell { get; set; }

        /// <summary>
        /// Width of the game area.
        /// </summary>
        public double GameWidth { get; }

        /// <summary>
        /// Height of the game area.
        /// </summary>
        public double GameHeight { get; }
    }
}
