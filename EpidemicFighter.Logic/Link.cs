﻿using System.Windows.Media;

namespace EpidemicFighter.Logic
{
    /// <summary>
    /// Visual and logical representation of an in-game link between two cells.
    /// </summary>
    public class Link : GameItem
    {
        public Link(Cell cell1, Cell cell2)
        {
            X = Y = 0;
            Cell1 = cell1;
            Cell2 = cell2;
            Distance = cell1.DistanceFrom(cell2);

            var geometry = new GeometryGroup();
            geometry.Children.Add(new LineGeometry(cell1.Center, cell2.Center));
            Area = geometry;
        }

        /// <summary>
        /// Source cell.
        /// </summary>
        public Cell Cell1 { get; set; }

        /// <summary>
        /// Destination cell.
        /// </summary>
        public Cell Cell2 { get; set; }

        /// <summary>
        /// Distance between the cells.
        /// </summary>
        public double Distance { get; set; }
    }
}
