﻿namespace EpidemicFighter.Logic
{
    /// <summary>
    /// Game configuration to be loaded from appsettings.json.
    /// </summary>
    public class Configuration
    {
        /// <summary>
        /// Storage connection string.
        /// </summary>
        public string? StorageConnectionString { get; set; }
    }
}
