﻿using Newtonsoft.Json;
using System.Windows;
using System.Windows.Media;

namespace EpidemicFighter.Logic
{
    /// <summary>
    /// Visual and logical representation of an in-game cell.
    /// </summary>
    public class Cell : GameItem
    {
        private readonly CellStrengthVisualization _strengthVisualization;

        public Cell(double x, double y, CellState state = CellState.Passive, double strength = 0)
        {
            X = x;
            Y = y;
            State = state;
            Strength = strength;

            var geometry = new GeometryGroup();
            geometry.Children.Add(new EllipseGeometry(new Point(0, 0), Constants.CellSize / 2, Constants.CellSize / 2));
            Area = geometry;

            _strengthVisualization = new CellStrengthVisualization(x, y);
        }

        /// <summary>
        /// Strength of the cell.
        /// </summary>
        public double Strength { get; set; }

        /// <summary>
        /// State of the cell.
        /// </summary>
        public CellState State { get; set; }

        public string Id => JsonConvert.SerializeObject((X, Y));

        /// <summary>
        /// Strengthens the cell by a predefined, constant unit.
        /// </summary>
        public void Strengthen()
        {
            if (Strength > 0)
            {
                Strength += Constants.StrenghteningPerTick;
                if (Strength > 1) Strength = 1;
            }
        }

        /// <summary>
        /// Checks whether the given cell can attack or support another cell.
        /// </summary>
        /// <param name="cell">The other cell to be attacked/supported.</param>
        /// <returns>True if the given cell can attack or support the other cell.</returns>
        public bool CanAttack(Cell cell)
        {
            // User cannot start attack from an infected or passive cell.
            if (State != CellState.Immune) return false;

            // Cell needs a minimum strength to attack.
            if (Strength < Constants.MinStrengthToStartAttact) return false;

            // Cannot attack/support itself.
            if (this == cell) return false;

            return true;
        }

        /// <summary>
        /// Gets the area of the strength visualization.
        /// </summary>
        /// <returns>Area of the strength visualization.</returns>
        public Geometry? GetStrengthVisualizationArea() => _strengthVisualization.GetArea(Strength);
    }
}
