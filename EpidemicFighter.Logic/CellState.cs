﻿namespace EpidemicFighter.Logic
{
    /// <summary>
    /// Possible states of a cell.
    /// </summary>
    public enum CellState
    {
        Passive,
        Immune,
        Infected,
    }
}
