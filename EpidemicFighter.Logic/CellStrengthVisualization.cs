﻿using System.Windows;
using System.Windows.Media;

namespace EpidemicFighter.Logic
{
    /// <summary>
    /// Visualization of the strength of a cell.
    /// </summary>
    public class CellStrengthVisualization : GameItem
    {
        private double? _strengthCache = null;

        public CellStrengthVisualization(double x, double y)
        {
            X = x;
            Y = y;
        }

        /// <summary>
        /// Gets the area of the strength visualization.
        /// </summary>
        /// <param name="strength">Strenght of the cell.</param>
        /// <returns>Area of the strength visualization.</returns>
        public Geometry? GetArea(double strength)
        {
            if (strength != _strengthCache)
            {
                ClearPositionCache();

                var geometry = new GeometryGroup();
                geometry.Children.Add(new EllipseGeometry(new Point(0, 0), Constants.CellSize / 2 * strength, Constants.CellSize / 2 * strength));
                Area = geometry;
            }
            _strengthCache = strength;

            return GetArea();
        }
    }
}
