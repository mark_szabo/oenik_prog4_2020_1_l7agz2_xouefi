var interface_epidemic_fighter_1_1_logic_1_1_i_game_logic =
[
    [ "AddLink", "interface_epidemic_fighter_1_1_logic_1_1_i_game_logic.html#ad0bb8555790f7aabe01d67818ae2ba90", null ],
    [ "AreCellsConnected", "interface_epidemic_fighter_1_1_logic_1_1_i_game_logic.html#a8023a4fd9a15a7df8f7b48e7a4070ff2", null ],
    [ "ComputeDistances", "interface_epidemic_fighter_1_1_logic_1_1_i_game_logic.html#af5f8f0e3bedf1f2a6f84b8dc039e0c53", null ],
    [ "ComputerAttack", "interface_epidemic_fighter_1_1_logic_1_1_i_game_logic.html#a780e951a8111b3b10c6b18de2068ec73", null ],
    [ "GetClickedCell", "interface_epidemic_fighter_1_1_logic_1_1_i_game_logic.html#a6582263b0b25c09a7e70badbf9f06aaa", null ],
    [ "OnCellClickHandler", "interface_epidemic_fighter_1_1_logic_1_1_i_game_logic.html#af9e56638331aa914c84e28f96419729a", null ],
    [ "RemoveAllLink", "interface_epidemic_fighter_1_1_logic_1_1_i_game_logic.html#aa2ff9902d4397c52d2feaf47f03987e1", null ],
    [ "RemoveLink", "interface_epidemic_fighter_1_1_logic_1_1_i_game_logic.html#aca0e7cce79d17fecac289b649a765d8c", null ],
    [ "Tick", "interface_epidemic_fighter_1_1_logic_1_1_i_game_logic.html#a96dcc1bf4eb9508a4cb0da0cc149ddd5", null ],
    [ "GameOver", "interface_epidemic_fighter_1_1_logic_1_1_i_game_logic.html#aded571ba3664019d08f35adb939da4fd", null ],
    [ "NextLevel", "interface_epidemic_fighter_1_1_logic_1_1_i_game_logic.html#a436cde9895a595ac741d6f8d8fae913c", null ],
    [ "Render", "interface_epidemic_fighter_1_1_logic_1_1_i_game_logic.html#a9a15d88b00401f12c785d59d327be6ae", null ]
];