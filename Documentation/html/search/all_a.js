var searchData=
[
  ['nextlevel_72',['NextLevel',['../class_epidemic_fighter_1_1_logic_1_1_game_logic.html#af72dd608cd9046f1bb32cca2e1cb0485',1,'EpidemicFighter.Logic.GameLogic.NextLevel()'],['../interface_epidemic_fighter_1_1_logic_1_1_i_game_logic.html#a436cde9895a595ac741d6f8d8fae913c',1,'EpidemicFighter.Logic.IGameLogic.NextLevel()']]],
  ['nickname_73',['NickName',['../class_epidemic_fighter_1_1_u_i_1_1_game.html#aefb9b756fb8343bd379e16d5ddce077b',1,'EpidemicFighter.UI.Game.NickName()'],['../class_epidemic_fighter_1_1_logic_1_1_game_model.html#a2bbc0bacf508d133ae5319e0ec548d11',1,'EpidemicFighter.Logic.GameModel.Nickname()'],['../class_epidemic_fighter_1_1_logic_1_1_saved_game_model.html#a70700416c058aaef2cbefd08dba545ae',1,'EpidemicFighter.Logic.SavedGameModel.Nickname()'],['../class_epidemic_fighter_1_1_repository_1_1_score_entity.html#a719e498580f376be7a3a12bbae5a6d99',1,'EpidemicFighter.Repository.ScoreEntity.Nickname()']]]
];
