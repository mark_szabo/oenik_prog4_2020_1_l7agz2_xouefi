var searchData=
[
  ['selectedcell_196',['SelectedCell',['../class_epidemic_fighter_1_1_logic_1_1_game_model.html#aa6b6b062582e35daf337c83da5127ccc',1,'EpidemicFighter.Logic.GameModel.SelectedCell()'],['../class_epidemic_fighter_1_1_logic_1_1_saved_game_model.html#ae21a2a56c478d351059e14a732791d4f',1,'EpidemicFighter.Logic.SavedGameModel.SelectedCell()']]],
  ['state_197',['State',['../class_epidemic_fighter_1_1_logic_1_1_cell.html#ab77e72ca7b7403e0afa0f2974e0a0a76',1,'EpidemicFighter::Logic::Cell']]],
  ['storageconnectionstring_198',['StorageConnectionString',['../class_epidemic_fighter_1_1_logic_1_1_configuration.html#a6cf666295b20120b09be057a65b5090c',1,'EpidemicFighter::Logic::Configuration']]],
  ['strength_199',['Strength',['../class_epidemic_fighter_1_1_logic_1_1_attack_link.html#a968d4cff6e7119bbe443e76a9226ea2b',1,'EpidemicFighter.Logic.AttackLink.Strength()'],['../class_epidemic_fighter_1_1_logic_1_1_cell.html#a0805029ef80813ae0c37da10cd4ec941',1,'EpidemicFighter.Logic.Cell.Strength()']]]
];
