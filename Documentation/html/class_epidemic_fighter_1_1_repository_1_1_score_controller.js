var class_epidemic_fighter_1_1_repository_1_1_score_controller =
[
    [ "ScoreController", "class_epidemic_fighter_1_1_repository_1_1_score_controller.html#a60e7b7e864e33b84585536b00ae3530f", null ],
    [ "AddOrUpdateScore", "class_epidemic_fighter_1_1_repository_1_1_score_controller.html#a38fe18f5049f8675dd8699806c0cd2a5", null ],
    [ "AddScore", "class_epidemic_fighter_1_1_repository_1_1_score_controller.html#a2f47f9918c423e161e5bd2a98533697a", null ],
    [ "GetScore", "class_epidemic_fighter_1_1_repository_1_1_score_controller.html#abddfeddc6fca6df6d10e379e109ef651", null ],
    [ "GetTop5", "class_epidemic_fighter_1_1_repository_1_1_score_controller.html#acf9a013160275ed1af6932920678e297", null ]
];