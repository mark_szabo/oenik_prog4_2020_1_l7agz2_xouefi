var class_epidemic_fighter_1_1_logic_1_1_cell =
[
    [ "Cell", "class_epidemic_fighter_1_1_logic_1_1_cell.html#ac48631f175e38e4157a52b1220e35a89", null ],
    [ "CanAttack", "class_epidemic_fighter_1_1_logic_1_1_cell.html#ac042b21904d936fe5f1e62d81f5a369c", null ],
    [ "GetStrengthVisualizationArea", "class_epidemic_fighter_1_1_logic_1_1_cell.html#a76ec62b76a88e099ed805e63eef500b5", null ],
    [ "Strengthen", "class_epidemic_fighter_1_1_logic_1_1_cell.html#add0024dc1f767bb85e3d0361bde626bf", null ],
    [ "Id", "class_epidemic_fighter_1_1_logic_1_1_cell.html#a1a47c5a5cf5e7a2796ba7321f077a04c", null ],
    [ "State", "class_epidemic_fighter_1_1_logic_1_1_cell.html#ab77e72ca7b7403e0afa0f2974e0a0a76", null ],
    [ "Strength", "class_epidemic_fighter_1_1_logic_1_1_cell.html#a0805029ef80813ae0c37da10cd4ec941", null ]
];