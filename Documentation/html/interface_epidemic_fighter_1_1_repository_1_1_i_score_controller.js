var interface_epidemic_fighter_1_1_repository_1_1_i_score_controller =
[
    [ "AddOrUpdateScore", "interface_epidemic_fighter_1_1_repository_1_1_i_score_controller.html#a89e2894d39ec91473eda549335400b15", null ],
    [ "AddScore", "interface_epidemic_fighter_1_1_repository_1_1_i_score_controller.html#ac930729990ad0856836a64c7e5103904", null ],
    [ "GetScore", "interface_epidemic_fighter_1_1_repository_1_1_i_score_controller.html#af9aa9439737c2af8f1f48956ddae75e8", null ],
    [ "GetTop5", "interface_epidemic_fighter_1_1_repository_1_1_i_score_controller.html#a1df7cb90d3a4f0b5485b77cb3aaca3e4", null ]
];