var class_epidemic_fighter_1_1_logic_1_1_saved_game_model =
[
    [ "SavedGameModel", "class_epidemic_fighter_1_1_logic_1_1_saved_game_model.html#a96f579fe5c27832e901c3d72b143bc21", null ],
    [ "SavedGameModel", "class_epidemic_fighter_1_1_logic_1_1_saved_game_model.html#a43940463543f15d1e163d55a51eef188", null ],
    [ "ToGameModel", "class_epidemic_fighter_1_1_logic_1_1_saved_game_model.html#ae422aec9d0f53c673d4156787bc4157b", null ],
    [ "ToJson", "class_epidemic_fighter_1_1_logic_1_1_saved_game_model.html#a5e39e597bff3a72b56fa7c6b5e5e913c", null ],
    [ "AttackLinks", "class_epidemic_fighter_1_1_logic_1_1_saved_game_model.html#a4b99982370fec5a0fc427aa3cb148672", null ],
    [ "Cells", "class_epidemic_fighter_1_1_logic_1_1_saved_game_model.html#a588e6bbebca2ae77e79f11bb17afe9cc", null ],
    [ "EllapsedSeconds", "class_epidemic_fighter_1_1_logic_1_1_saved_game_model.html#a99e7ca8b671bb55f99caddee2fe17fb3", null ],
    [ "GameHeight", "class_epidemic_fighter_1_1_logic_1_1_saved_game_model.html#a38f654eee1b10fb1272aa0cacdcc8946", null ],
    [ "GameWidth", "class_epidemic_fighter_1_1_logic_1_1_saved_game_model.html#a62af6049747abe07aabeac8760d8db5c", null ],
    [ "Level", "class_epidemic_fighter_1_1_logic_1_1_saved_game_model.html#a90eff60c1b344b854da34ce73c9dcec8", null ],
    [ "Links", "class_epidemic_fighter_1_1_logic_1_1_saved_game_model.html#a2cd412f0e18efd41d7bec72aa6f9f6cd", null ],
    [ "Nickname", "class_epidemic_fighter_1_1_logic_1_1_saved_game_model.html#a70700416c058aaef2cbefd08dba545ae", null ],
    [ "SelectedCell", "class_epidemic_fighter_1_1_logic_1_1_saved_game_model.html#ae21a2a56c478d351059e14a732791d4f", null ]
];