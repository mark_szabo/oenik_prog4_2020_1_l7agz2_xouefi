var hierarchy =
[
    [ "Application", null, [
      [ "EpidemicFighter.UI.App", "class_epidemic_fighter_1_1_u_i_1_1_app.html", null ]
    ] ],
    [ "EpidemicFighter.UI.Common", "class_epidemic_fighter_1_1_u_i_1_1_common.html", null ],
    [ "EpidemicFighter.Logic.Configuration", "class_epidemic_fighter_1_1_logic_1_1_configuration.html", null ],
    [ "EpidemicFighter.Logic.GameItem", "class_epidemic_fighter_1_1_logic_1_1_game_item.html", [
      [ "EpidemicFighter.Logic.Cell", "class_epidemic_fighter_1_1_logic_1_1_cell.html", null ],
      [ "EpidemicFighter.Logic.CellStrengthVisualization", "class_epidemic_fighter_1_1_logic_1_1_cell_strength_visualization.html", null ],
      [ "EpidemicFighter.Logic.Link", "class_epidemic_fighter_1_1_logic_1_1_link.html", [
        [ "EpidemicFighter.Logic.AttackLink", "class_epidemic_fighter_1_1_logic_1_1_attack_link.html", null ]
      ] ]
    ] ],
    [ "EpidemicFighter.Logic.Tests.GameLogicTests", "class_epidemic_fighter_1_1_logic_1_1_tests_1_1_game_logic_tests.html", null ],
    [ "EpidemicFighter.Logic.GameModel", "class_epidemic_fighter_1_1_logic_1_1_game_model.html", null ],
    [ "IComponentConnector", null, [
      [ "EpidemicFighter.UI.SetYourName", "class_epidemic_fighter_1_1_u_i_1_1_set_your_name.html", null ]
    ] ],
    [ "EpidemicFighter.Logic.IGameLogic", "interface_epidemic_fighter_1_1_logic_1_1_i_game_logic.html", [
      [ "EpidemicFighter.Logic.GameLogic", "class_epidemic_fighter_1_1_logic_1_1_game_logic.html", null ]
    ] ],
    [ "InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "EpidemicFighter.Repository.IScoreController", "interface_epidemic_fighter_1_1_repository_1_1_i_score_controller.html", [
      [ "EpidemicFighter.Repository.ScoreController", "class_epidemic_fighter_1_1_repository_1_1_score_controller.html", null ]
    ] ],
    [ "EpidemicFighter.Repository.ITableStorageController< T >", "interface_epidemic_fighter_1_1_repository_1_1_i_table_storage_controller.html", [
      [ "EpidemicFighter.Repository.TableStorageController< T >", "class_epidemic_fighter_1_1_repository_1_1_table_storage_controller.html", null ]
    ] ],
    [ "EpidemicFighter.Logic.Map", "class_epidemic_fighter_1_1_logic_1_1_map.html", null ],
    [ "Page", null, [
      [ "EpidemicFighter.UI.GameOver", "class_epidemic_fighter_1_1_u_i_1_1_game_over.html", null ]
    ] ],
    [ "Page", null, [
      [ "EpidemicFighter.UI.Game", "class_epidemic_fighter_1_1_u_i_1_1_game.html", null ],
      [ "EpidemicFighter.UI.HallOfFame", "class_epidemic_fighter_1_1_u_i_1_1_hall_of_fame.html", null ],
      [ "EpidemicFighter.UI.Menu", "class_epidemic_fighter_1_1_u_i_1_1_menu.html", null ],
      [ "EpidemicFighter.UI.SetYourName", "class_epidemic_fighter_1_1_u_i_1_1_set_your_name.html", null ]
    ] ],
    [ "EpidemicFighter.Logic.SavedGameModel", "class_epidemic_fighter_1_1_logic_1_1_saved_game_model.html", null ],
    [ "EpidemicFighter.Logic.Score", "class_epidemic_fighter_1_1_logic_1_1_score.html", null ],
    [ "TableEntity", null, [
      [ "EpidemicFighter.Repository.ScoreEntity", "class_epidemic_fighter_1_1_repository_1_1_score_entity.html", null ]
    ] ],
    [ "EpidemicFighter.Repository.TableStorageController< ScoreEntity >", "class_epidemic_fighter_1_1_repository_1_1_table_storage_controller.html", [
      [ "EpidemicFighter.Repository.ScoreController", "class_epidemic_fighter_1_1_repository_1_1_score_controller.html", null ]
    ] ],
    [ "Window", null, [
      [ "EpidemicFighter.UI.MainWindow", "class_epidemic_fighter_1_1_u_i_1_1_main_window.html", null ]
    ] ]
];