var class_epidemic_fighter_1_1_logic_1_1_game_item =
[
    [ "ClearPositionCache", "class_epidemic_fighter_1_1_logic_1_1_game_item.html#a80e2c81f10ffd0efa459ead9f8a8fce7", null ],
    [ "CollidesWith", "class_epidemic_fighter_1_1_logic_1_1_game_item.html#ae37752853c623fc009c3a29e47872210", null ],
    [ "DistanceFrom", "class_epidemic_fighter_1_1_logic_1_1_game_item.html#a3f4dfa8cbe8f16c3c9d033e5e5101803", null ],
    [ "GetArea", "class_epidemic_fighter_1_1_logic_1_1_game_item.html#a75512b2d855f4f39fce468be38b9973e", null ],
    [ "Area", "class_epidemic_fighter_1_1_logic_1_1_game_item.html#a079012a97772aff5c3cc50cbce76f0ea", null ],
    [ "Center", "class_epidemic_fighter_1_1_logic_1_1_game_item.html#a05a0a0d74e0c3236d7328509fd60d2c1", null ],
    [ "X", "class_epidemic_fighter_1_1_logic_1_1_game_item.html#a6a9ee6ac390bfdd3b07532cdbdb1360b", null ],
    [ "Y", "class_epidemic_fighter_1_1_logic_1_1_game_item.html#a83fc3bcf988292dc568d47a1d259c787", null ]
];