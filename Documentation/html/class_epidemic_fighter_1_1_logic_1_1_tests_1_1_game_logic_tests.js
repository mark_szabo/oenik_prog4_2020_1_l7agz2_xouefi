var class_epidemic_fighter_1_1_logic_1_1_tests_1_1_game_logic_tests =
[
    [ "GameLogicTests", "class_epidemic_fighter_1_1_logic_1_1_tests_1_1_game_logic_tests.html#ab1ce0ecb08bf01b7ddf3e92b55ebc73c", null ],
    [ "AddLink_ByDefault_AddsLinksToDictionary", "class_epidemic_fighter_1_1_logic_1_1_tests_1_1_game_logic_tests.html#a96827d6fe641bef846f0dca9c95fe87b", null ],
    [ "AreCellsConnected_WithCellsConnected_ReturnsTrue", "class_epidemic_fighter_1_1_logic_1_1_tests_1_1_game_logic_tests.html#a4cea79fe1de9171ec04afabf79bd920c", null ],
    [ "AreCellsConnected_WithCellsNotConnected_ReturnsFalse", "class_epidemic_fighter_1_1_logic_1_1_tests_1_1_game_logic_tests.html#a28f757ac47068ae5be41282b74781374", null ],
    [ "GetClickedCell_ByDefault_ReturnsClickedCell", "class_epidemic_fighter_1_1_logic_1_1_tests_1_1_game_logic_tests.html#aeaa7109b3a0d484e8af79fbc1150c966", null ],
    [ "GetClickedCell_WithNoCellsClicked_ReturnsNull", "class_epidemic_fighter_1_1_logic_1_1_tests_1_1_game_logic_tests.html#a75f95c00711498a8ab4301870547e883", null ],
    [ "RemoveLink_ByDefault_RemovesLinksFromDictionary", "class_epidemic_fighter_1_1_logic_1_1_tests_1_1_game_logic_tests.html#aa26c34438ec3e9117e68485e58df4271", null ],
    [ "RemoveLink_WithNonExistentLink_DoesNothing", "class_epidemic_fighter_1_1_logic_1_1_tests_1_1_game_logic_tests.html#a204402a5f43ef08c8805487c36443989", null ],
    [ "Setup", "class_epidemic_fighter_1_1_logic_1_1_tests_1_1_game_logic_tests.html#a82d3cac422785bb2b8ab0a12cfaac5cb", null ],
    [ "Tick_ByDefault_RaisesEvent", "class_epidemic_fighter_1_1_logic_1_1_tests_1_1_game_logic_tests.html#aa6753f134b519dbfa3b1ce0c40065a85", null ],
    [ "Tick_ByDefault_StrengthensCells", "class_epidemic_fighter_1_1_logic_1_1_tests_1_1_game_logic_tests.html#ae714973fd82821d4bc12a5d859393f35", null ]
];