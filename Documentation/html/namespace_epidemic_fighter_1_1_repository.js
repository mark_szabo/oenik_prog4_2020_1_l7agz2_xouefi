var namespace_epidemic_fighter_1_1_repository =
[
    [ "IScoreController", "interface_epidemic_fighter_1_1_repository_1_1_i_score_controller.html", "interface_epidemic_fighter_1_1_repository_1_1_i_score_controller" ],
    [ "ITableStorageController", "interface_epidemic_fighter_1_1_repository_1_1_i_table_storage_controller.html", "interface_epidemic_fighter_1_1_repository_1_1_i_table_storage_controller" ],
    [ "ScoreController", "class_epidemic_fighter_1_1_repository_1_1_score_controller.html", "class_epidemic_fighter_1_1_repository_1_1_score_controller" ],
    [ "ScoreEntity", "class_epidemic_fighter_1_1_repository_1_1_score_entity.html", "class_epidemic_fighter_1_1_repository_1_1_score_entity" ],
    [ "TableStorageController", "class_epidemic_fighter_1_1_repository_1_1_table_storage_controller.html", "class_epidemic_fighter_1_1_repository_1_1_table_storage_controller" ]
];