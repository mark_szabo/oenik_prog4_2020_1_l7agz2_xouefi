var class_epidemic_fighter_1_1_repository_1_1_table_storage_controller =
[
    [ "TableStorageController", "class_epidemic_fighter_1_1_repository_1_1_table_storage_controller.html#abbb173fc34626381f78fd06477d99b7d", null ],
    [ "DeleteEntity", "class_epidemic_fighter_1_1_repository_1_1_table_storage_controller.html#a764275baeae3dc4eb6394402773db2e6", null ],
    [ "GetAllEntity", "class_epidemic_fighter_1_1_repository_1_1_table_storage_controller.html#ade8c1116315423dc621295698674ca16", null ],
    [ "InsertOrMergeEntity", "class_epidemic_fighter_1_1_repository_1_1_table_storage_controller.html#a94de7929313e9757bec1913dc1de33bb", null ],
    [ "RetrieveEntityUsingPointQuery", "class_epidemic_fighter_1_1_repository_1_1_table_storage_controller.html#a481a2b21b5f8b1567d7e38a0a9783970", null ],
    [ "_tableClient", "class_epidemic_fighter_1_1_repository_1_1_table_storage_controller.html#a4c66846e63bfc9d9cd3f1567737833a4", null ]
];