using NUnit.Framework;
using System.Collections.Generic;
using System.Windows;

namespace EpidemicFighter.Logic.Tests
{
    [TestFixture]
    public class GameLogicTests
    {
        private GameModel _model;
        private GameLogic _logic;

        public GameLogicTests()
        {
            var map = new Map
            {
                Cells = new List<Cell>
                {
                    new Cell(150, 500, CellState.Immune, 0.5),
                    new Cell(300, 330),
                    new Cell(300, 660),
                    new Cell(550, 400),
                    new Cell(550, 600),
                }
            };
            _model = new GameModel(map, Constants.Width, Constants.Height);
            _logic = new GameLogic(_model);
        }

        [SetUp]
        public void Setup()
        {
            var map = new Map
            {
                Cells = new List<Cell>
                {
                    new Cell(150, 500, CellState.Immune, 0.5),
                    new Cell(300, 330),
                    new Cell(300, 660),
                    new Cell(550, 400),
                    new Cell(550, 600),
                }
            };
            _model = new GameModel(map, Constants.Width, Constants.Height);
            _logic = new GameLogic(_model);
        }

        [Test]
        public void Tick_ByDefault_StrengthensCells()
        {
            var cell1 = _model.Cells[0];
            var cell2 = _model.Cells[1];

            _logic.Tick();

            Assert.AreEqual(0.5 + Constants.StrenghteningPerTick, cell1.Strength);
            Assert.AreEqual(0, cell2.Strength);
        }

        [Test]
        public void Tick_ByDefault_RaisesEvent()
        {
            var eventRaised = false;
            _logic.Render += (_, __) => eventRaised = true;

            _logic.Tick();

            Assert.IsTrue(eventRaised);
        }

        [Test]
        public void AddLink_ByDefault_AddsLinksToDictionary()
        {
            var cell1 = _model.Cells[0];
            var cell2 = _model.Cells[1];
            var cell3 = _model.Cells[2];

            _logic.AddLink(cell1, cell2);
            _logic.AddLink(cell1, cell3);

            Assert.IsTrue(_model.Links[cell2].ContainsKey(cell1));
            Assert.IsTrue(_model.Links[cell1].ContainsKey(cell2));
            Assert.IsTrue(_model.Links[cell3].ContainsKey(cell1));
            Assert.IsTrue(_model.Links[cell1].ContainsKey(cell3));
        }

        [Test]
        public void RemoveLink_ByDefault_RemovesLinksFromDictionary()
        {
            var cell1 = _model.Cells[0];
            var cell2 = _model.Cells[1];
            _model.Links = new Dictionary<Cell, Dictionary<Cell, Link>>
            {
                { cell1, new Dictionary<Cell, Link> { { cell2, new Link(cell1, cell2) } } },
                { cell2, new Dictionary<Cell, Link> { { cell1, new Link(cell2, cell1) } } },
            };

            _logic.RemoveLink(cell1, cell2);


            Assert.IsTrue(!_model.Links[cell2].ContainsKey(cell1));
            Assert.IsTrue(!_model.Links[cell1].ContainsKey(cell2));
        }

        [Test]
        public void RemoveLink_WithNonExistentLink_DoesNothing()
        {
            var cell1 = _model.Cells[0];
            var cell2 = _model.Cells[1];
            var cell3 = _model.Cells[2];
            _model.Links = new Dictionary<Cell, Dictionary<Cell, Link>>
            {
                { cell1, new Dictionary<Cell, Link> { { cell2, new Link(cell1, cell2) } } },
                { cell2, new Dictionary<Cell, Link> { { cell1, new Link(cell2, cell1) } } },
            };

            _logic.RemoveLink(cell1, cell3);
            _logic.RemoveLink(cell3, cell1);

            Assert.IsTrue(_model.Links[cell2].ContainsKey(cell1));
            Assert.IsTrue(_model.Links[cell1].ContainsKey(cell2));
        }

        [Test]
        public void AreCellsConnected_WithCellsConnected_ReturnsTrue()
        {
            var cell1 = _model.Cells[0];
            var cell2 = _model.Cells[1];
            _model.Links = new Dictionary<Cell, Dictionary<Cell, Link>>
            {
                { cell1, new Dictionary<Cell, Link> { { cell2, new Link(cell1, cell2) } } },
                { cell2, new Dictionary<Cell, Link> { { cell1, new Link(cell2, cell1) } } },
            };

            var areConnected = _logic.AreCellsConnected(cell1, cell2);
            var areConnectedFromOtherDirection = _logic.AreCellsConnected(cell2, cell1);

            Assert.IsTrue(areConnected);
            Assert.IsTrue(areConnectedFromOtherDirection);
        }

        [Test]
        public void AreCellsConnected_WithCellsNotConnected_ReturnsFalse()
        {
            var cell1 = _model.Cells[0];
            var cell2 = _model.Cells[1];
            var cell3 = _model.Cells[2];
            _model.Links = new Dictionary<Cell, Dictionary<Cell, Link>>
            {
                { cell1, new Dictionary<Cell, Link> { { cell2, new Link(cell1, cell2) } } },
                { cell2, new Dictionary<Cell, Link> { { cell1, new Link(cell2, cell1) } } },
            };

            var areConnected = _logic.AreCellsConnected(cell1, cell3);
            var areConnectedFromOtherDirection = _logic.AreCellsConnected(cell3, cell1);

            Assert.IsTrue(!areConnected);
            Assert.IsTrue(!areConnectedFromOtherDirection);
        }

        [Test]
        public void GetClickedCell_ByDefault_ReturnsClickedCell()
        {
            var cell1 = _model.Cells[0];
            var point = new Point(153, 505);

            var clickedCell = _logic.GetClickedCell(point);

            Assert.AreEqual(cell1, clickedCell);
        }

        [Test]
        public void GetClickedCell_WithNoCellsClicked_ReturnsNull()
        {
            var point = new Point(170, 540);

            var clickedCell = _logic.GetClickedCell(point);

            Assert.AreEqual(null, clickedCell);
        }
    }
}