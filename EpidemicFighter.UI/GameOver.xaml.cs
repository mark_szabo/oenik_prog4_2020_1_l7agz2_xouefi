﻿using Microsoft.Azure.Cosmos.Table;
using System.Windows;
using System.Windows.Controls;

namespace EpidemicFighter.UI
{
    /// <summary>
    /// Interaction logic for GameOver.xaml
    /// </summary>
    public partial class GameOver : Page
    {
        private readonly CloudTable _cloudTable;
        public GameOver(CloudTable cloudTable)
        {
            this._cloudTable = cloudTable;
            InitializeComponent();
        }

        public GameOver(CloudTable cloudTable, string result, int score)
        {
            this._cloudTable = cloudTable;
            InitializeComponent();
            resultLabel.Content = $"{result}\nScore: {score}";
        }

        /// <summary>
        /// Load the menu page.
        /// </summary>
        /// <param name="sender">_</param>
        /// <param name="e">_</param>
        private void BacktoMenuLabel_MouseLeftButtonUp(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Menu(_cloudTable));
        }
    }
}
