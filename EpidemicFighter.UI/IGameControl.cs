using System.Windows;

namespace EpidemicFighter.UI
{
    /// <summary>
    /// Game control.
    /// </summary>
    internal interface IGameControl
    {
        /// <summary>
        /// Gets called the first time the screen is loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void GameScreen_Loaded(object sender, RoutedEventArgs e);
    }
}
