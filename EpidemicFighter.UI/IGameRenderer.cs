using System.Windows.Media;

namespace EpidemicFighter.UI
{
    /// <summary>
    /// Game renderer.
    /// </summary>
    internal interface IGameRenderer
    {
        /// <summary>
        /// Renders the screen.
        /// </summary>
        /// <param name="context">Drawing context.</param>
        void Render(DrawingContext context);
    }
}
