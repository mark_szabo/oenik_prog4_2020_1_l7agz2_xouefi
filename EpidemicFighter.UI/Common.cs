﻿using System;
using Microsoft.Azure.Cosmos.Table;

namespace EpidemicFighter.UI
{
    public class Common
    {
        /// <summary>
        /// Creates cloud storage account from connection string.
        /// </summary>
        /// <param name="storageConnectionString">Connection string.</param>
        /// <returns>Returns storage account.</returns>
        private static CloudStorageAccount CreateStorageAccountFromConnectionString(string storageConnectionString)
        {
            CloudStorageAccount storageAccount;
            try
            {
                storageAccount = CloudStorageAccount.Parse(storageConnectionString);
            }
            catch (FormatException f)
            {
                throw new Exception(f.Message);
            }
            catch (ArgumentException a)
            {
                throw new Exception(a.Message);
            }

            return storageAccount;
        }

        /// <summary>
        /// Create cloud table from connection string.
        /// </summary>
        /// <param name="connectionString">Connection string.</param>
        /// <param name="tableName">Name of the Cloud Table</param>
        /// <returns>Returns cloud table.</returns>
        public static CloudTable CreateTable(string connectionString, string tableName)
        {
            CloudStorageAccount storageAccount = CreateStorageAccountFromConnectionString(connectionString);

            CloudTableClient tableClient = storageAccount.CreateCloudTableClient(new TableClientConfiguration());

            CloudTable table = tableClient.GetTableReference(tableName);

            return table;
        }
    }
}
