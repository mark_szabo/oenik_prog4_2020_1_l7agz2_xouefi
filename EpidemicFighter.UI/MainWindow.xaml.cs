﻿using EpidemicFighter.Logic;
using Microsoft.Azure.Cosmos.Table;
using Microsoft.Extensions.Configuration;
using System;
using System.Windows;

namespace EpidemicFighter.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Configuration? _configuration;
        private CloudTable? _cloudTable;

        public MainWindow()
        {
            InitializeComponent();

            Loaded += MainWindow_Loaded;
        }

        /// <summary>
        /// Inisialize cloud table and the config file if the window loaded.
        /// </summary>
        /// <param name="sender">_</param>
        /// <param name="e">_</param>
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var window = GetWindow(this);
            if (window != null)
            {
                _configuration = new ConfigurationBuilder()
                    .AddJsonFile("appsettings.json")
                    .Build()
                    .Get<Configuration>();

                if (_configuration?.StorageConnectionString == null) throw new Exception("Cloud storage connection string was not found.");

                _cloudTable = Common.CreateTable(_configuration.StorageConnectionString, "scores");

                frame.NavigationService.Navigate(new Menu(_cloudTable));
            }
        }
    }
}
