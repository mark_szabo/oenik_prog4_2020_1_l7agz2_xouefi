﻿using EpidemicFighter.Logic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Diagnostics;
using Microsoft.Azure.Cosmos.Table;
using System.Linq;

namespace EpidemicFighter.UI
{
    /// <summary>
    /// Game control.
    /// </summary>
    internal class GameControl : FrameworkElement
    {
        private GameModel? _model;
        private IGameLogic? _logic;
        private IGameRenderer? _renderer;
        private DispatcherTimer? _timer;
        private DispatcherTimer? _computerAttackTimer;
        private int _level = 1;
        private readonly List<Map> _maps;
        private readonly Stopwatch _stopwatch = Stopwatch.StartNew();
        private int _score = 0;

        public GameControl()
        {
            _maps = MapService.LoadMaps();

            Loaded += (_, __) => GameScreen_Loaded();
            Unloaded += GameControl_Unloaded;
        }

        /// <summary>
        /// Exports the current game model to JSON.
        /// </summary>
        /// <param name="sender">_</param>
        /// <param name="e">_</param>
        private void GameControl_Unloaded(object sender, RoutedEventArgs e)
        {
            if (_model?.Cells.Any(c => c.State == CellState.Immune) ?? false) ExportGameToJson();
        }

        /// <summary>
        /// Gets called the first time the screen is loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GameScreen_Loaded()
        {
            var tempModel = ImportSavedGameFromJson();

            if (tempModel != null) _model = new GameModel(tempModel, ActualWidth, ActualHeight);
            else
            {
                var name = GetName() ?? throw new Exception("Nickname cannot be null!");
                _model = new GameModel(_maps[_level - 1], ActualWidth, ActualHeight, name);
            }

            _logic = new GameLogic(_model);
            _renderer = new GameRenderer(_model);

            var window = Window.GetWindow(this);
            if (window != null)
            {
                window.MouseDown += Window_MouseDown;

                _timer = new DispatcherTimer
                {
                    Interval = TimeSpan.FromMilliseconds(100)
                };
                _timer.Tick += (_, __) => _logic.Tick();
                _timer.Start();
                _computerAttackTimer = new DispatcherTimer
                {
                    Interval = TimeSpan.FromMilliseconds(5000)
                };
                _computerAttackTimer.Tick += (_, __) => _logic.ComputerAttack();
                _computerAttackTimer.Start();

                _stopwatch.Restart();
            }

            _logic.ComputeDistances();

            _logic.Render += (_, __) => InvalidateVisual();
            _logic.GameOver += GameOver;
            _logic.NextLevel += LoadNextLevel;
        }

        /// <summary>
        /// Load the next level.
        /// </summary>
        /// <param name="sender">_</param>
        /// <param name="e">_</param>
        private void LoadNextLevel(object? sender, EventArgs e)
        {
            var tempScore = _maps[_level - 1].MaxScore - (Convert.ToInt32(_stopwatch.ElapsedMilliseconds / 1000) + _model?.EllapsedSeconds) ?? 0;
            _score += tempScore <= 0 ? 0 : tempScore;

            if (_level == _maps.Count) GameOver(sender, e);
            else
            {
                _level += 1;
                GameScreen_Loaded();
            }
        }

        /// <summary>
        /// Load the game over screen and upload the score and the nick name to the Table Storage.
        /// </summary>
        /// <param name="sender">_</param>
        /// <param name="e">_</param>
        private void GameOver(object? sender, EventArgs e)
        {
            File.Delete("save.json");

            var cloudTable = GetCloudTable() ?? throw new Exception("Cloud table not found!");

            Score scoreService = new Score(cloudTable);

            if (_model?.Nickname == null)
            {
                var name = GetName() ?? throw new Exception("Nick name not found!");
                scoreService.AddScore(name, _score);
            }
            else
            {
                scoreService.AddScore(_model.Nickname, _score);
            }

            var parent = VisualTreeHelper.GetParent(this);
            while (!(parent is Page))
            {
                parent = VisualTreeHelper.GetParent(parent);
            }
            if (parent != null)
            {
                (parent as Game)?.LoadGameOverPage((_level == _maps.Count ? "You completed this game!" : "You lost, the virus has killed you!"), _score);
            }
            else
            {
                throw new Exception("Parent page cannot be null!");
            }
        }

        /// <summary>
        /// Gets the nick name from the game page.
        /// </summary>
        /// <returns>Returns the nick name of the player.</returns>
        private string? GetName()
        {
            var parent = VisualTreeHelper.GetParent(this);
            while (!(parent is Page))
            {
                parent = VisualTreeHelper.GetParent(parent);
            }
            if (parent != null)
            {
                return (parent as Game)?.NickName;
            }
            else
            {
                throw new Exception("Parent page cannot be null!");
            }
        }

        /// <summary>
        /// Gets the cloud table from the game page.
        /// </summary>
        /// <returns>Returns a cloud table.</returns>
        private CloudTable? GetCloudTable()
        {
            var parent = VisualTreeHelper.GetParent(this);
            while (!(parent is Page))
            {
                parent = VisualTreeHelper.GetParent(parent);
            }
            if (parent != null)
            {
                return (parent as Game)?.CloudTable;
            }
            else
            {
                throw new Exception("Parent page cannot be null!");
            }
        }

        /// <summary>
        /// Gets called on each render.
        /// </summary>
        /// <param name="drawingContext">Drawing context.</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            _renderer?.Render(drawingContext);
        }

        /// <summary>
        /// Gets called on a mouse down (aka. click) event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var point = e.GetPosition(this);
            var clickedCell = _logic?.GetClickedCell(point);
            if (clickedCell != null) _logic?.OnCellClickHandler(clickedCell);
        }

        /// <summary>
        /// Export the current game to a json file.
        /// </summary>
        public void ExportGameToJson()
        {
            if (_model == null) return;
            var savedModel = new SavedGameModel(_model);

            using StreamWriter swriter = new StreamWriter(new FileStream("save.json", FileMode.Create, FileAccess.ReadWrite), Encoding.UTF8);
            swriter.Write(savedModel.ToJson(_stopwatch));
        }

        /// <summary>
        /// Import a saved game from json file.
        /// </summary>
        /// <returns>Returns a game model.</returns>
        public static GameModel? ImportSavedGameFromJson()
        {
            if (File.Exists("save.json"))
            {
                using var reader = new StreamReader(new FileStream("save.json", FileMode.Open, FileAccess.Read), true);
                var json = reader.ReadToEnd();

                var savedModel = JsonConvert.DeserializeObject<SavedGameModel>(json);

                return savedModel.ToGameModel();
            }

            return null;
        }
    }
}
