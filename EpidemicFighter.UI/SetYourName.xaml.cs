﻿using Microsoft.Azure.Cosmos.Table;
using System.Windows;
using System.Windows.Controls;

namespace EpidemicFighter.UI
{
    /// <summary>
    /// Interaction logic for SetYourName.xaml
    /// </summary>
    public partial class SetYourName : Page
    {
        private readonly CloudTable _cloudTable;
        public SetYourName(CloudTable cloudTable)
        {
            this._cloudTable = cloudTable;
            InitializeComponent();
        }

        /// <summary>
        /// Load the game screen.
        /// </summary>
        /// <param name="sender">_</param>
        /// <param name="e">_</param>
        private void StartLabel_MouseLeftButtonUp(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Game(_cloudTable, nameInput.Text.Trim()));
        }

        /// <summary>
        /// Loads the menu page.
        /// </summary>
        /// <param name="sender">_</param>
        /// <param name="e">_</param>
        private void BackToMenuLabel_MouseLeftButtonUp(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Menu(_cloudTable));
        }
    }
}
