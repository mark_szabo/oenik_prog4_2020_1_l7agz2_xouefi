﻿using EpidemicFighter.Logic;
using System;
using System.Windows;
using System.Windows.Media;

namespace EpidemicFighter.UI
{
    /// <summary>
    /// Game renderer.
    /// </summary>
    internal class GameRenderer : IGameRenderer
    {
        private readonly GameModel _model;
        private Rect _background;
        private readonly Pen _cellPen = new Pen(Constants.CellColor, 2);
        private readonly Pen _immuneCellPen = new Pen(Constants.ImmuneCellColor, 2);
        private readonly Pen _infectedCellPen = new Pen(Constants.InfectedCellColor, 2);
        private readonly Pen _selectedCellPen = new Pen(Constants.SelectedCellColor, 2);

        public GameRenderer(GameModel model)
        {
            _model = model;
            _background = new Rect(0, 0, model.GameWidth, model.GameHeight);
        }

        /// <summary>
        /// Renders the screen.
        /// </summary>
        /// <param name="context">Drawing context.</param>
        public void Render(DrawingContext context)
        {
            // Render background.
            context.DrawRectangle(Constants.BackgroundColor, null, _background);

            // Render permanent links.
            foreach (var linkGroup in _model.Links)
            {
                var cellColor = linkGroup.Key.State switch
                {
                    CellState.Immune => Constants.ImmuneCellColor,
                    CellState.Infected => Constants.InfectedCellColor,
                    CellState.Passive => Constants.CellColor,
                    _ => throw new ArgumentException(message: "Invalid enum value.", paramName: nameof(linkGroup.Key.State)),
                };

                var cellPen = linkGroup.Key.State switch
                {
                    CellState.Immune => _immuneCellPen,
                    CellState.Infected => _infectedCellPen,
                    CellState.Passive => _cellPen,
                    _ => throw new ArgumentException(message: "Invalid enum value.", paramName: nameof(linkGroup.Key.State)),
                };

                foreach (var link in linkGroup.Value)
                {
                    context.DrawGeometry(cellColor, cellPen, link.Value.GetArea());
                }
            }

            // Render temporary attack/support links.
            foreach (var attackLink in _model.AttackLinks)
            {
                var cellColor = attackLink.Cell1.State switch
                {
                    CellState.Immune => Constants.ImmuneCellColor,
                    CellState.Infected => Constants.InfectedCellColor,
                    CellState.Passive => Constants.CellColor,
                    _ => throw new ArgumentException(message: "Invalid enum value.", paramName: nameof(attackLink.Cell1.State)),
                };

                var cellPen = attackLink.Cell1.State switch
                {
                    CellState.Immune => _immuneCellPen,
                    CellState.Infected => _infectedCellPen,
                    CellState.Passive => _cellPen,
                    _ => throw new ArgumentException(message: "Invalid enum value.", paramName: nameof(attackLink.Cell1.State)),
                };

                context.DrawGeometry(cellColor, cellPen, attackLink.GetArea());
            }

            // Render cells.
            foreach (var cell in _model.Cells)
            {
                // Render a cell.
                var cellColor = cell.State switch
                {
                    CellState.Immune => Constants.BackgroundColor,
                    CellState.Infected => Constants.BackgroundColor,
                    CellState.Passive => Constants.CellColor,
                    _ => throw new ArgumentException(message: "Invalid enum value.", paramName: nameof(cell.State)),
                };

                var cellPen = _model.SelectedCell == cell ?
                    _selectedCellPen :
                    cell.State switch
                    {
                        CellState.Immune => _immuneCellPen,
                        CellState.Infected => _infectedCellPen,
                        CellState.Passive => _cellPen,
                        _ => throw new ArgumentException(message: "Invalid enum value.", paramName: nameof(cell.State)),
                    };

                context.DrawGeometry(cellColor, cellPen, cell.GetArea());

                // Render the strength visualization of the cell.
                cellColor = cell.Strength < Constants.MinStrengthToStartAttact ?
                    Constants.CellColor :
                    cell.State switch
                    {
                        CellState.Immune => Constants.ImmuneCellColor,
                        CellState.Infected => Constants.InfectedCellColor,
                        CellState.Passive => Constants.CellColor,
                        _ => throw new ArgumentException(message: "Invalid enum value.", paramName: nameof(cell.State)),
                    };

                cellPen = cell.Strength < Constants.MinStrengthToStartAttact ?
                    _cellPen :
                    cell.State switch
                    {
                        CellState.Immune => _immuneCellPen,
                        CellState.Infected => _infectedCellPen,
                        CellState.Passive => _cellPen,
                        _ => throw new ArgumentException(message: "Invalid enum value.", paramName: nameof(cell.State)),
                    };

                context.DrawGeometry(cellColor, cellPen, cell.GetStrengthVisualizationArea());
            }
        }
    }
}
