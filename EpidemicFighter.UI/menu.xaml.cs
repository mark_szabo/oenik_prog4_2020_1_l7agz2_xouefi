﻿using Microsoft.Azure.Cosmos.Table;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace EpidemicFighter.UI
{
    /// <summary>
    /// Interaction logic for Menu.xaml
    /// </summary>
    public partial class Menu : Page
    {
        private readonly CloudTable _cloudTable;
        public Menu(CloudTable cloudTable)
        {
            this._cloudTable = cloudTable;
            InitializeComponent();

            Loaded += Menu_Loaded;
        }

        /// <summary>
        /// Sets the game start label based on previous game load.
        /// </summary>
        /// <param name="sender">_</param>
        /// <param name="e">_</param>
        private void Menu_Loaded(object sender, RoutedEventArgs e)
        {
            if (File.Exists("save.json")) gameStartLabel.Content = "Resume Game";
            else gameStartLabel.Content = "New Game";
        }

        /// <summary>
        /// Closes the game.
        /// </summary>
        /// <param name="sender">_</param>
        /// <param name="e">_</param>
        private void ExitLabel_MouseLeftButtonUp(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        /// <summary>
        /// Load the Hall of Fame page.
        /// </summary>
        /// <param name="sender">_</param>
        /// <param name="e">_</param>
        private void HallOfFameLabel_MouseLeftButtonUp(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new HallOfFame(_cloudTable));
        }

        /// <summary>
        /// Navigates two different page, if the save json exists then load the game page, if not then load the name setter page.
        /// </summary>
        /// <param name="sender">_</param>
        /// <param name="e">_</param>
        private void NewGameLabel_MouseLeftButtonUp(object sender, RoutedEventArgs e)
        {
            if (File.Exists("save.json")) this.NavigationService.Navigate(new Game(_cloudTable));
            else this.NavigationService.Navigate(new SetYourName(_cloudTable));
        }
    }
}
