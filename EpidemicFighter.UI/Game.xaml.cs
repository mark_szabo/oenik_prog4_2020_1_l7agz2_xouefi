﻿using Microsoft.Azure.Cosmos.Table;
using System.Windows;
using System.Windows.Controls;

namespace EpidemicFighter.UI
{
    /// <summary>
    /// Interaction logic for Game.xaml
    /// </summary>
    public partial class Game : Page
    {
        /// <summary>
        /// Cloud table.
        /// </summary>
        public CloudTable CloudTable { get; }

        /// <summary>
        /// Nickname of the player.
        /// </summary>
        public string? NickName { get; set; }

        public Game(CloudTable cloudTable)
        {
            CloudTable = cloudTable;
            InitializeComponent();
        }

        public Game(CloudTable cloudTable, string name)
        {
            CloudTable = cloudTable;
            this.NickName = name;
            InitializeComponent();
        }

        /// <summary>
        /// Load the game over page.
        /// </summary>
        /// <param name="result">Result of the game.</param>
        /// <param name="score">Reached score.</param>
        public void LoadGameOverPage(string result, int score)
        {
            this.NavigationService?.Navigate(new GameOver(CloudTable, result, score));
        }

        /// <summary>
        /// Load menu page.
        /// </summary>
        /// <param name="sender">_</param>
        /// <param name="e">_</param>
        private void BacktoMenuLabel_MouseLeftButtonUp(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Menu(CloudTable));
        }
    }
}
