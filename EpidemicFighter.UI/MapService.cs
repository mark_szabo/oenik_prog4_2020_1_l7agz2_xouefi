﻿using EpidemicFighter.Logic;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace EpidemicFighter.UI
{
    /// <summary>
    /// Map service.
    /// </summary>
    internal static class MapService
    {
        private const string Path = "maps.json";

        /// <summary>
        /// Loads maps from file.
        /// </summary>
        /// <returns>Loaded maps.</returns>
        public static List<Map> LoadMaps()
        {
            using var reader = new StreamReader(Path);
            var json = reader.ReadToEnd();
            var maps = JsonConvert.DeserializeObject<List<Map>>(json);

            return maps;
        }
    }
}
