﻿using System;
using System.Windows;
using System.Windows.Controls;
using EpidemicFighter.Logic;
using Microsoft.Azure.Cosmos.Table;

namespace EpidemicFighter.UI
{
    /// <summary>
    /// Interaction logic for HallOfFame.xaml
    /// </summary>
    public partial class HallOfFame : Page
    {
        private readonly CloudTable _cloudTable;

        public HallOfFame(CloudTable cloudTable)
        {
            this._cloudTable = cloudTable;
            InitializeComponent();
            PrintScore();
        }

        /// <summary>
        /// Loads the menu page.
        /// </summary>
        /// <param name="sender">_</param>
        /// <param name="e">_</param>
        private void BacktoMenuLabel_MouseLeftButtonUp(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Menu(_cloudTable));
        }

        /// <summary>
        /// Closes the game.
        /// </summary>
        /// <param name="sender">_</param>
        /// <param name="e">_</param>
        private void ExitLabel_MouseLeftButtonUp(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        /// <summary>
        /// Show the scores.
        /// </summary>
        public void PrintScore()
        {
            Score scoreService = new Score(_cloudTable);
            var scores = scoreService.GetScore();

            string outputText = "";
            foreach (var score in scores)
            {
                if (score.Nickname == null || score.Point == null) continue;
                outputText += StringFiller(score.Nickname, score.Point.Value.ToString(), 60) + "\n";
            }

            ScoreLabel.Content = outputText;
        }

        /// <summary>
        /// Appends two string and aligns them with dots.
        /// </summary>
        /// <param name="text1">Text 1</param>
        /// <param name="text2"> Text 2</param>
        /// <param name="length">Caracter limit</param>
        /// <returns>Retursn the appended string.</returns>
        public string StringFiller(string text1, string text2, int length)
        {
            if ((text1+text2).Length <= length)
            {
                int lengthText1 = text1.Length;
                int lengthText2 = text2.Length;

                string filler = "";

                for (int i = 0; i < (length - lengthText1 - lengthText2); i++) filler += ".";

                return $"{text1}{filler}{text2}";
            }
            else
            {
                throw new Exception("Invalid input");
            }
        }
    }
}
