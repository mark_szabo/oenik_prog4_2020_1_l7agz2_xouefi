# epidemic-fighter

Homework in C# for the Programming 4 course of Obuda University

![Mockup](mockup.jpg)

## Gameplay

You control the blue cells and your task is to fight a virus which has infected the red cells.
You start with one cell and there is one infected cell.
To win, you need to deactivate all red cells.
Your cells are getting stronger over time, but the infected cells behave the same way as well.
To attack an infected cell, just drag from one of your cells.
If you take one of the empty cells or deactivate one of the infected cells, the strength of your node will be dispersed between the two.
Distance is costly - attacking cells far away will make you loose some of your strength just by getting there. The farther away the target cell is, the higher the cost will be.

**Hurry up and contain the epidemic!**
